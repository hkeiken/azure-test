#!/bin/bash

echo "parsing azuredeploy.parameters"
json2yaml -o yaml/azureDeploy.parameters.yaml original/azureDeploy.parameters.json
echo "parsing azuredeploy"
json2yaml -o yaml/azuredeploy.yaml original/azuredeploy.json         
echo "parsing createRBAC"
json2yaml -o yaml/createRBAC.yaml original/createRBAC.json 
echo "parsing staging-vm"
json2yaml -o yaml/staging-vm.yaml original/staging-vm.json           
echo "parsing vng-vm"
json2yaml -o yaml/vng-vm.yaml original/vng-vm.json
