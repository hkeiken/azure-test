# testing vagrant1


Arm template testing vagrant1



[Start in Azure 20190121](https://portal.azure.com/#create/Microsoft.Template/uri/https%3A%2F%2Fbitbucket.org%2Fhkeiken%2Fazure-test%2Fraw%2Fmaster%2F201901%2F20190121.json)


Untested list vagrant:

[vSRX-Square](https://github.com/JNPRAutomate/vSRX-Square)
A vSRX Vagrant Topology for testing interaction between multiple vSRX devices

[vSRX-Passthrough](https://github.com/JNPRAutomate/vSRX-Passthrough)
A vSRX Vagrant topology for passthrough testing

[vSRXHA-Passthrough](https://github.com/JNPRAutomate/vSRXHA-Passthrough)
vSRX HA Passthrough Lab

[vSRX-BackToBack](https://github.com/JNPRAutomate/vSRX-BackToBack)
vSRX Back to Back Vagrant Topology

[vSRX-SnL](https://github.com/JNPRAutomate/vSRX-SnL)
vSRX Spine and Leaf Topology with 7 VMs

[vagrant-demo-scenaries](https://github.com/JNPRAutomate/vagrant-demo-scenarios)

[vqfx10k-vagrant](https://github.com/Juniper/vqfx10k-vagrant)
Vagrant projects for vQFX10k

[Juniper vSRX Vagrant Libvirt Box Installation](https://codingpackets.com/blog/juniper-vsrx-vagrant-libvirt-box-install/)

[Juniper vQFX Vagrant Libvirt Box Installation](https://codingpackets.com/blog/juniper-vqfx-vagrant-libvirt-box-install/)

[On-demand Juniper labs using Vagrant](http://www.fredrikholmberg.com/2016/04/on-demand-juniper-labs-using-vagrant/)

Various info:

[Multiple Vagrant VMs in One Vagrantfile](http://www.thisprogrammingthing.com/2015/multiple-vagrant-vms-in-one-vagrantfile/)
