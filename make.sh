#!/bin/bash

#Combine yaml files to a big json ready for import to azure
cat vsrx.sorted.1-default.yaml vsrx.sorted.2-parameters.yaml vsrx.sorted.3-resources.yaml \
vsrx.sorted.4-variables.yaml  |\
 yaml2json --indent-json -o json/vsrx.sorted.json

#Check for errors in json file
jsonlint json/vsrx.sorted.json -q

#Convert json ready for import to azure
yaml2json --indent-json -i vsrx.parameters.sorted.yaml -o json/vsrx.parameters.sorted.json 

#Check for errors in json file
jsonlint json/vsrx.parameters.sorted.json -q


yaml2json --indent-json -o json/modified-staging-vm.parameters.json modified-staging-vm.parameters.yaml
jsonlint json/modified-staging-vm.parameters.json -q
yaml2json --indent-json -o json/modified-staging-vm.json modified-staging-vm.yaml
jsonlint json/modified-staging-vm.json -q
