# Very simple deployment of a Linux VM

[Original](https://github.com/Azure/azure-quickstart-templates/tree/master/101-vm-simple-linux)

[launch](https://portal.azure.com/#create/Microsoft.Template/uri/https%3A%2F%2Fwww.bitbucket.com%2Fhkeiken%2Fazure-test%2F201901d%2Fazuredeploy.json)

This template allows you to deploy a simple Linux VM using a few different options for the Ubuntu Linux version, using the latest patched version. This will deploy a A1 size VM in the resource group location and return the FQDN of the VM.
